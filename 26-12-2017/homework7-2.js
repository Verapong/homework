let fs = require('fs');

// Promise คือ เป็นตัวเทนของค่า ที่ไม่รู้ว่าเมื่อใหร่จะได้ และไม่รู้ว่าจะได้จริงๆ หรือเปล่า
// เช่น การอ่านไฟล์

// แปลงฟังก์ชั่นที่ใช้ Callback มาเป็นฟังก์ชั้นที่ส่ง Promise กลับไป
function readFile(name) {
    return new Promise((resolve, reject) => {
        fs.readFile(name, 'utf-8', function (err, data) {
            if (err) {
                // Reject คือ มี Error
                reject(err)
            } else {
                resolve(data)
            }
        })
    })
}

function addYearSalary(row) {
    row.yearSalary = row.salary * 12
    return row
}

function addNextSalary(row) {
    row.nextSalary = [row.salary, (row.salary * 1.1), (row.salary * 1.1)*1.1]
    return row
}

function addAdditionalFields(employees) {
    // return employees.map(addYearSalary).map(addNextSalary)
    let result = employees.map(function(person) {//ชี้ไปที่employeeก่อน
        addNextSalary(person)
        addYearSalary(person)
        let newObj = {}
        for(let i in person) {//ชี้ไปที่person อยู่แล้วเลยวนเลยไม่ต้องไปเขียน for loop อีกรอบเพื่อเข้าไปที่ employeesอีกครั้ง ใช้ประโยคจากการที่มันเข้ามาใน employees อยู่แล้ว
            newObj[i] = person[i]

        }
        return newObj
    })
    return result
}

async function main() {
    const file = await readFile('homework1.JSON')
    const employees = JSON.parse(file)
    // const employee = employees[0]
    // const result = addYearSalary(employee)
    // const result2 = addNextSalary(employee)
    // console.log('Result2', result2)

    const newEmployee = addAdditionalFields(employees)
    employees[0]["salary"] = 0
    employees[1]["salary"] = 0
    employees[2]["salary"] = 0
    employees[3]["salary"] = 0
    console.log(employees)
    console.log(newEmployee)
    return employees
}

main()