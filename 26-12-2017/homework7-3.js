let Input1 = [1, 2, 3]
let Input2 = { a: 1, b: 2 }
let Input3 = [1, 2, { a: 1, b: 2 }]
let Input4 = [1, 2, { a: 1, b: { c: 3, d: 4 } }]

// if (Array.isArray(obj)) {

// } else if (typeof obj == "object") {

// } else {

// }

function cloneobject(obj) {
    let newObj
    if (Array.isArray(obj)) {
        newObj = []
        for (let a in obj) {
            if (typeof obj[a] == "object") {
                let newObj_B = {}
                for (let b in obj[a]) {
                    let newObj_C
                    if (typeof obj[a][b] == "object") {
                        let newObj_C = {}
                        for (let c in obj[a][b]) {
                            newObj_C[c] = obj[a][b][c]
                        }
                        newObj_B[b] = newObj_C
                    } else {
                        newObj_B[b] = obj[a][b]
                    }
                }
                newObj[a] = newObj_B
            } else {
                newObj[a] = obj[a]
            }
        }
    } else if (typeof obj == "object") {
        newObj = {}
        for (let a in obj) {
            newObj[a] = obj[a]
        }
    }
    return newObj
}

let cloneObj = cloneobject(Input4)

//Input4[2]["b"]["d"] = 99;
Input4[2].a = 99;
console.log(Input4)
console.log(cloneObj)

//let clone = JSON.parse(JSON.stringify(Input4))