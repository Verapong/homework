let fs = require('fs');

// Promise คือ เป็นตัวเทนของค่า ที่ไม่รู้ว่าเมื่อใหร่จะได้ และไม่รู้ว่าจะได้จริงๆ หรือเปล่า
// เช่น การอ่านไฟล์

// แปลงฟังก์ชั่นที่ใช้ Callback มาเป็นฟังก์ชั้นที่ส่ง Promise กลับไป
function readFile(name) {
    return new Promise((resolve, reject) => {
        fs.readFile(name, 'utf-8', function (err, data) {
            if (err) {
                // Reject คือ มี Error
                reject(err)
            } else {
                resolve(data)
            }
        })
    })
}

function addYearSalary(row) {
    row.yearSalary = row.salary * 12
    return row
}

function addNextSalary(row) {
    row.nextSalary = [row.salary, (row.salary * 1.1), (row.salary * 1.1)*1.1]
    return row
}

function addAdditionalFields(employees) {
    return employees.map(addYearSalary).map(addNextSalary)
}

async function main() {
    const file = await readFile('homework1.JSON')
    const employees = JSON.parse(file)
    // const employee = employees[0]
    // const result = addYearSalary(employee)
    // const result2 = addNextSalary(employee)
    // console.log('Result2', result2)

    const result = addAdditionalFields(employees)
    console.log(result)
    return employees
}

main()