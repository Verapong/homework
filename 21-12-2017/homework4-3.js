let fs = require('fs');
fs.readFile('homework1.json', 'utf8', function(err, data) {
    let filesalary = JSON.parse(data)
    
    const changedSalary = filesalary.map(data => {
        if (data.salary < 100000) {
            data.salary = data.salary*2
        }
           
        return data
    })

    const poorSalary = changedSalary.filter(data => data.salary < 100000)
    console.log('poorsalary', poorSalary)

    const sumsalary = changedSalary
        .map(data => data.salary)
        .reduce((sum, salary) => sum + salary, 0)

    console.log('sumsalary', sumsalary)
})
