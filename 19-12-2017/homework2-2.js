let fields = ['id','firstname','lastname','company','salary'];
let employees = [
    ['1001','Luke','Skywalker','Walt Disney','40000'],
    ['1002','Tony','Stark','Marvel','1000000'],
    ['1003','Somchai','Jaidee','Love2work','20000'],
    ['1004','Monkey D','Luffee','One Piece','9000000']
];
let result=[];
for (let i in employees){
   let a = employees[i]
   let newfields = {};
   for (let i in a){
       newfields[fields[i]] = a[i]
   }
   result.push(newfields);
}
console.log(result);
