let assert = require('assert');
let add = require('./test1.js');
console.log(add);
let myAdd = add.myAdd


describe('tdd lab',function(){
    describe('#add()',function(){
        it ('should calculate  1+1=2',function(){
            assert.equal(myAdd(1,1),2);

        })
        it ('should calculate  1+2=3',function(){
            assert.equal(myAdd(1,2),3);

        })
        it ('should calculate  3+2=5',function(){
            assert.equal(myAdd(3,2),5);

        })
        it ('should calculate  100+50=150',function(){
            assert.equal(myAdd(100,50),150);

        })
    })
})