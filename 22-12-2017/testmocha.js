let assert=require('assert');
let fs=require('fs');
function myReadFile(filename,callback){
    fs.readFile(filename,'utf8',function(err,dataDemo1){
        if (err)
        callback(err,null);
        else 
        callback(null,dataDemo1)
    })
}

describe('Async Test',function(){
    describe('#myReadFile()',function(){
        it('should be equal to this object structure',function(done){
            myReadFile("body.txt",(err,data)=>{
                myReadFile("body2.txt",(err,data)=>{

                })
                if (err)
                done(err);
                else{
                    assert.strictEqual(data.length>0,true,"demofile1.txt should not be empty");
                    done();
                }
            })
        })
    })
})