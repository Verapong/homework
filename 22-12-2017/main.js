function add(a,b) {
    return a+b;
}
function addAsync(a,b, callback) {
    let result = 0;
    setTimeout(function () {
        result = add(a,b);
        callback(result);
    },1000);
    return result;
}
let result = add(1,2);
let result2 = addAsync(2,2, function(result){
    console.log('this is result from async', result)
});
console.log(result);
console.log(result2);

let {myAdd} = require('./test1');