let fs = require('fs')
const { Employee } = require('./employee.js');
const { Programmer } = require('./Programmer.js');

class CEO extends Employee {
    constructor(firstname, lastname, salary, id, dressCode) {//somshi
        super(firstname, lastname, salary);//somchi
        this.dressCode = dressCode 
        this.id = id
        this.dressCodeAtOffice = 'tshirt'
        this.employees = []
        let self = this
    }

    getSalary() { // simulate public method
        return super.getSalary() * 2;
    }

    work(employee) { // simulate public method
        this._fire(employee)
        this._hire(employee)
        this._seminar()
        this._golf()
    }
 
    _fire(employee) {
        console.log(employee.firstname + ' has been fired! Dress with : ' + this.dressCodeAtOffice)
    }

    _hire(employee) {
        console.log(employee.firstname + ' has been hired back! Dress with : ' + this.dressCodeAtOffice)
    }

    _seminar() {
        console.log('He is going to seminar Dress with :' + this.dressCode)
    }

    increaseSalary(employee, newSalary) {
        if (employee.setSalary(newSalary)) {
            console.log(employee.firstname + ' salary has been set to ' + employee._salary)
        } else {
            console.log(employee.firstname + ' salary is less than before!!! ')
        }
    }

    _golf() { // simulate private method
        this.dressCode = 'golf_dress';
        console.log("He goes to golf club to find a new connection." + " Dress with :" + this.dressCode);

    }

    talk(message) {

        console.log(message)

    }

    reportRobot(self, message) {

        self.talk(message)
    }
    readFile() {
        return new Promise(function (resolve, reject) {
            fs.readFile("homework1.JSON", 'utf-8', function (err, data) {
                if (err) {
                    reject(err)
                } else {
                    resolve(data)
                }
            })
        })
    }

    async convert() {

        try {
            this.employeeRaw = JSON.parse(await this.readFile())
            this.CopyData(this.employeeRaw)
        }
        catch (error) {
            console.error(error)
        }
    }

    CopyData(employeeraw) {
        for (let i = 0; i < employeeraw.length; i++) {
            let newClass = new Programmer(employeeraw[i]["firstname"],
                employeeraw[i]["Lastname"],
                employeeraw[i]["salary"],
                employeeraw[i]["id"],
                "Front-End")
            this.employees.push(newClass)
        }
        console.log(this.employees)
    }
}

exports.CEO = CEO;