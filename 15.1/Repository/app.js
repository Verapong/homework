const mysql2 = require('mysql2/promise')
const User = require('./Repository')

const pool = mysql2.createPool({
    host: 'localhost',
    user:'root',
    database:'codecamp'
})
async function query(){
    const db = await pool.getConnection()
    

    const u = await User.find(db,1001)
    u.firstname = 'hacker00'
    User.store(db,u)
   
    const User1 = await User.find(db,1001)
    User1.firstname = 'tester'
    User.store(db,User1)

    const User2 = await User.findByUsername('Tony',db) 
    console.log(User2)

    const User3 = await User.findAll(db)
    console.log(User3)

    const User4 = await User.remove (db,1001)
}

query()