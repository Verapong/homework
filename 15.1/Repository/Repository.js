function createEntity (row) {
    return {
        id:row.id,
        firstname: row.firstname,
        lastname: row.lastname,
    }
}
async function find(db,id) {
    const [rows] = await db.execute(`select id,firstname,
    lastname from user where id = ? `,[id])
    return createEntity(rows[0])
}
async function findAll(db) {
    const [rows] = await db.execute(`select id,firstname,
    lastname from user  `,)
    console.log(rows)
    return rows.map(row => createEntity(row))
}
async function findByUsername(firstname,db){
    const [rows] = await db.execute('select id,firstname, lastname from user where firstname =  ?' , [firstname])
    return rows.map(row => createEntity(row))

   /*  for (let i = 0; i< rows.length; i++) {
        let row = rows[i]
        return [ row.id , row.firstname, row.lastname]
    } */
    
    //const userbyname = user.findByUsername('Tony')
   //const userbyname = rows.map((row) => new User(db,row))
}
async function store (db,user) {
    
    if (!user.id){
        const result = await db.execute(`insert into user (id,firstname,lastname) values(?,?)`,[user.firstname,user.lastname])
        user.id = result.insertId
        return
    }
return  db.execute(`update user set firstname = ?, lastname = ? where id = ?`,[user.firstname,user.lastname,user.id])
}
function remove (db,id){
    return db.execute(`delete from user where id = ?`,[id])

}

module.exports = {
    find,
    findAll,
    findByUsername,
    store,
    remove
}