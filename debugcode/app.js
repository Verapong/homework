const koa = require('koa')
const app = new koa();
const logger = require('koa-logger');

let winston = require('winston');
const winLog = winston.createLogger({
    level: 'info',
    format: winston.format.json(),
    transports: [
        new winston.transports.File({ filename: 'error.log', level: 'error' }),
        new winston.transports.File({ filename: 'combined.log' })
    ]
});
winLog.add(new winston.transports.Console({
    format: winston.format.simple()
}));

app.use(async (ctx, next) => {
    winLog.log({level: 'info', message: ctx})
    next()
})

app.use(async (ctx, next) => {
    ctx.body = 'Hello World';
    await next();
})


app.listen(3000);
app.use(logger());