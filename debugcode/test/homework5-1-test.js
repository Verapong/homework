// import assert
let assert = require('assert');
// Import Module
let fs = require('fs');

let myFunc = require("../homework5-1.js");



describe('tdd lab', function() {
  describe('#checkFileExist()', function() {
        it('should have homework5-1_eyes.json existed', function(done) {
            myFunc.checkFileExist('homework5-1_eyes.json', function (err, data) {
                if (err) {
                    done(err);
                } else {
                    assert.strictEqual(data, true, "Found homework5-1_eyes.json should be");
                    done();
                }
            });
        });
    
        it('should have homework5-1_gender.json existed', function(done) {
            myFunc.checkFileExist('homework5-1_gender.json', function (err, data) {
                if (err) {
                    done(err);
                } else {
                    done();
                }
            });
        });
        it('should have homework5-1_friends.json existed', function(done) {
            myFunc.checkFileExist('homework5-1_friends.json', function (err, data) {
                if (err) {
                    done(err);
                } else {
                    done();
                }
            });
        });
    });
    describe('#objectKey()', function() {
        it('should have same object key stucture as homework5-1_eyes.json', function(done) {
            myFunc.readFile('homework5-1_eyes.json', function (err, data) {
                if (err) {
                    done(err);
                    return
                }

                let eyesKeys = ['brown', 'green', 'blue']

                assert.deepEqual(Object.keys(data), eyesKeys, 'Eyes Keys is not Equal')
                done()
            })
        });
        it('should have same object key stucture as homework5-1_friends.json', function(done) {
            myFunc.readFile('homework5-1_friends.json', function (err, data) {
                if (err){
                    done(err)
                }

                let friendsKeys = ['_id','friendCount','tagsCount','balance'];
                assert.deepEqual(Object.keys(data[0]), friendsKeys, 'Friends Keys is not Equal')
                done()
            });    
        });
        it('should have same object key stucture as homework5-1_gender.json', function(done) {
            myFunc.readFile('homework5-1_gender.json', function (err,data) {
                let genderKeys = ['male','female']

                assert.deepEqual(Object.keys(data), genderKeys, '')
                done()
            });
        });
    });
    describe('#userFriendCount()', function() {
        it('should have size of array input as 23', (done) => {
            myFunc.readFile('homework5-1_friends.json', function (err,data) {
                console.log('Reading homework5-1_friends.json', data)
                
                if (err) {
                    done(err)
                }
                assert.deepEqual(data.length, 23, 'should have size of array input as 23')
                done()
            });      
        }); 
    });
    describe('#sumOfEyes()', function() {
        it('should have sum of eyes as 23', function(done) {
            myFunc.readFile('homework5-1_eyes.json', function (err,data) {
                if (err) {
                    done(err)
                }

                let sum = Object.values(data)
                .reduce((total,data)=> {
                    return total + data;
                });
                assert.deepEqual(sum, 23, 'should have sum of eyes as 23')
                done()
            });
        });
    });

    describe('#sumOfGender()', function () {
        it('should have sum of gender as 23', function (done) {
            myFunc.readFile('homework5-1_gender.json', function (err, data) {
                if (err) {
                    // Done(err): จบการทำงาน แต่มีปัญหา
                    done(err)
                }

                let sum = Object.values(data).reduce((total, data) => {
                    return total + data;
                });
                assert.deepEqual(sum, 23, 'should have sum of gender as 23')

                // Done: จบการทำงาน
                done()
            });
        });

    });
})