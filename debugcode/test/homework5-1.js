let fs = require('fs');
// dataStr = JSON.parse(data);

let checkFileExist = function(filename,callback) {
    fs.access(filename, function(err) {
        if(err){
            callback(err, null);
        }else {
            callback(null, true);
        }
    });
}
let readFile = function (name, callback) {
    fs.readFile(name, 'utf8', function (err, data) {
        if (err) {
            callback(err)
        } else {
            callback(null, JSON.parse(data))
        }
    });
}

function countEyeColor() {
    readFile('homework1-4.json', function(err, data) {
        const eyeColors = data.map(item => item.eyeColor)
        const result = { brown: 0, green: 0, blue: 0 }

        eyeColors.forEach(value => {
            result[value] += 1
        })
 
        console.log(result)
        fs.writeFile('homework5-1_eyes.json',JSON.stringify(result),'utf8',function(err) {
           if (err)
             return(err)
           else
            return true
        });
    })
}



function countGender() {
    readFile('homework1-4.json', function (err, data) {
        const gender = data.map(function(item) {
            let a = {gender: item.gender, eyeColor: item.eyeColor}
            return a 
        })
        
        const result = {
            male: { 
                brown: 0,
                green: 0,
                blue: 0
            },
            female: {
                brown: 0,
                green: 0,
                blue: 0
            }
        }
        gender.forEach(x =>{
            result[x.gender][x.eyeColor]++
            // full version: result[x.gender][x.eyeColor] = result[x.gender][x.eyeColor] + 1
        })

        fs.writeFile('homework5-1_gender.json', JSON.stringify(result, null, 4), 'utf8', function (err) {
            if (err)
                return (err)
            else
                return true
        });
    })
}

function countFriends() {
    
    readFile('homework1-4.json', function (err, data) {
        const friends = data.map(item => {
         return {
             '_id' : item._id ,'friendCount' : item.friends.length, 
             'tagsCount' : item.tags.length, // tags = [afjejfe,eqfefe,efefef]
             'balance' : item.balance
            }
        })
        console.log(friends)
        fs.writeFile('homework5-1_friends.json', JSON.stringify(friends), 'utf8', function (err) {
            if (err)
                return (err)
            else
                return true
        });
    })  
}
    

countEyeColor()
countGender()
countFriends()

exports.checkFileExist = checkFileExist;
exports.readFile = readFile;