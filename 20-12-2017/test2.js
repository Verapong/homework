let peopleSalary = [
    { "id": 1001, "firstname": "Luke", "Lastname": "Skywalker", "company": "Walt Disney", "salary": 40000 },
    { "id": 1002, "firstname": "Tony", "Lastname": "Stark", "company": "Marvel", "salary": 1000000 },
    { "id": 1003, "firstname": "Somchai", "Lastname": "Jaidee", "company": "Love2work", "salary": 20000 },
    { "id": 1004, "firstname": "Monkey D", "Lastname": "Luffee", "company": "One Piece", "salary": 9000000 }
];

let newPeopleSalary = new Array(); // [] equals new Array()

for (index in peopleSalary) { // Getting index of array 0, 1, 2, 3
    // console.log("index: " + index);
    // 0 , 1,2,3
    // {"id":1001,"firstname":"Luke","Lastname":"Skywalker","company":"Walt Disney","salary":40000},
    let newObject = new Object(); // {}
    for (property in peopleSalary[index]) { // Getting propertyName of JSON eg. id, firstname, Lastname, company, salary
        if (property !== "company") {
            newObject[property] = peopleSalary[index][property];
        }
    }
    //newObject = {"id":1001,"firstname":"Luke","Lastname":"Skywalker","salary":40000},
    // newPeopleSalary = []
    newPeopleSalary.push(newObject);
    // newPeopleSalary = [{"id":1001,"firstname":"Luke","Lastname":"Skywalker","salary":40000}]
}


console.log("newPeopleSalary", newPeopleSalary);

// ["a","b","c"]
// {"name":"a"}

