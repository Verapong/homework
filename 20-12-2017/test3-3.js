let students =
[
    {"id":"1001",'firstname':'Luke','lastname':'Skywalker'},
    {"id":"1002",'firstname':'Tony','lastname':'Stark'},
    {"id":"1003",'firstname':'Somchai','lastname':'Jaidee'},
    {"id":"1004",'firstname':'Monkey D','lastname':'Luffee'},
];

let company = [
    {"id":"1001","company":"Walt Disney"},
    {"id":"1002","company":"Marvel"},
    {"id":"1003","company":"Love2work"},
    {"id":"1004","company":"One Piece"},
];
let salary = [
    {"id":"1001","salary":"40000"},
    {"id":"1002","salary":"1000000"},
    {"id":"1003","salary":"20000"},
    {"id":"1004","salary":"9000000"},
];
let like = [
    {"id":"1001","like":"apple"},
    {"id":"1002","like":"banana"},
    {"id":"1003","like":"orange"},
    {"id":"1004","like":"papaya"},
];
let dislike = [
    {"id":"1001","dislike":"banana"},
    {"id":"1002","dislike":"orange"},
    {"id":"1003","dislike":"papaya"},
    {"id":"1004","dislike":"apple"},
];
let employeeDatabase = [];
for (let a in students) { // a เข้าไปใน student[] 
    let objStudents = {} //กำหนด obj ว่าง วนลูปตามจำนวนนักเรียนคือมี4คนใน []มี4id
    for (const keyStudent in students[a]) { // students[a] เอา obj { }ออก        
        //console.log( keyStudent + ": " + students[a][keyStudent])
        objStudents[keyStudent] = students[a][keyStudent] // objStudents[keyStudent] = id fname lname, students[a][keyStudent] = ค่าของ id fname lname จะได้เป็น objStudents.id = 1001
    }
    
    for (const keyCompany in company[a]) {
        objStudents[keyCompany] = company[a][keyCompany]
    }
    for (const keySalary in salary[a]) {
        //console.log( keySalary + ": " + salary[a][keySalary])
        objStudents[keySalary] = salary[a][keySalary]
    }
    for (const keyLike in like[a]) {
        //console.log( keyLike + ": " + like[a][keyLike])
        objStudents[keyLike] = like[a][keyLike]
    }
    for (const keyDislike in dislike[a]) {
        //console.log( keyDislike + ": " + dislike[a][keyDislike])
        objStudents[keyDislike] = dislike[a][keyDislike]
    }
    //console.log(objStudents)
    employeeDatabase.push(objStudents) // เก็บไว้ใน objStudents
}

console.log(employeeDatabase)
employeeDatabase = JSON.stringify(employeeDatabase) 

let fs = require('fs');
function writenewtext(str) {
    return new Promise(function (resolve, reject) {
        fs.writeFile('employeeDatabase.js', str,'utf8', function (err) {
            if (err)
                reject(err);
            else
                resolve();
        })
    })
}

