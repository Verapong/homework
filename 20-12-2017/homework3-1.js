let fs = require('fs');
function head() {
    return new Promise(function (resolve, reject) {
        fs.readFile('head.txt', 'utf8', function (err, datahead) {
            if (err) {
                reject(err)
            } else {
                resolve(datahead)
            }
        });
    });
}
function body() {
    return new Promise(function (resolve, reject) {
        fs.readFile('body.txt', 'utf8', function (err, databody) {
            if (err) {
                reject(err)
            } else {
                resolve(databody)
            }
        });
    });
}
function leg() {
    return new Promise(function (resolve, reject) {
        fs.readFile('leg.txt', 'utf8', function (err, dataleg) {
            if (err) {
                reject(err)
            } else {
                resolve(dataleg)
            }
        });
    });
}
function feet() {
    return new Promise(function (resolve, reject) {
        fs.readFile('feet.txt', 'utf8', function (err, datafeet) {
            if (err) {
                reject(err)
            } else {
                resolve(datafeet)
            }
        });
    });
}

Promise.all([head(), body(), leg(), feet()])
    .then(function (result) {

        let newtext = ""
        for (i = 0; i < result.length; i++) {
            newtext += result[i]+"\n"
            console.log(newtext)
        }
       return newtext;
    })
    .then(function (newtext) {
        return writenewtext(newtext);
    })
    .catch(function (err) {
        // ..do something
    })
//console.log(newtext)
function writenewtext(str) {
    return new Promise(function (resolve, reject) {
        fs.writeFile('robot.txt', str, function (err) {
            if (err)
                reject(err);
            else
                resolve();
        })
    })
}