let fs = require('fs');
function head() {
    return new Promise(function (resolve, reject) {
        fs.readFile('head.txt', 'utf8', function (err, datahead) {
            if (err) {
                reject(err)
            } else {
                resolve(datahead)
            }
        });
    });
}
function body() {
    return new Promise(function (resolve, reject) {
        fs.readFile('body.txt', 'utf8', function (err, databody) {
            if (err) {
                reject(err)
            } else {
                resolve(databody)
            }
        });
    });
}
function leg() {
    return new Promise(function (resolve, reject) {
        fs.readFile('leg.txt', 'utf8', function (err, dataleg) {
            if (err) {
                reject(err)
            } else {
                resolve(dataleg)
            }
        });
    });
}
function feet() {
    return new Promise(function (resolve, reject) {
        fs.readFile('feet.txt', 'utf8', function (err, datafeet) {
            if (err) {
                reject(err)
            } else {
                resolve(datafeet)
            }
        });
    });
}

async function human(){
   
    try{
        let newtext=""
        let a = await head();
        let b = await body();
        let c = await leg();
        let d = await feet();
        newtext = a+"\n"+b+"\n"+c+"\n"+d+"\n";
        console.log(newtext)
        writenewtext(newtext);
    }catch (error){
    console.error(error)
}
}
human();
function writenewtext(str) {
    return new Promise(function (resolve, reject) {
        fs.writeFile('robot.txt', str, function (err) {
            if (err)
                reject(err);
            else
                resolve();
        })
    })
}
