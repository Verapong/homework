let students =
[
    { "id": "1001", 'firstname': 'Luke', 'lastname': 'Skywalker' },
    { "id": "1002", 'firstname': 'Tony', 'lastname': 'Stark' },
    { "id": "1003", 'firstname': 'Somchai', 'lastname': 'Jaidee' },
    { "id": "1004", 'firstname': 'Monkey D', 'lastname': 'Luffee' },
];

let salary = [
{ "id": "1001", "salary": "40000" },
{ "id": "1002", "salary": "1000000" },
{ "id": "1003", "salary": "20000" },
{ "id": "1004", "salary": "9000000" },
];
let like = [
{ "id": "1001", "like": "apple" },
{ "id": "1002", "like": "banana" },
{ "id": "1003", "like": "orange" },
{ "id": "1004", "like": "papaya" },
];
let dislike = [
{ "id": "1001", "dislike": "banana" },
{ "id": "1002", "dislike": "orange" },
{ "id": "1003", "dislike": "papaya" },
{ "id": "1004", "dislike": "apple" },
];



let company = [
{ "id": "1001", "company": "Walt Disney" },
{ "id": "1002", "company": "Marvel" },
{ "id": "1003", "company": "Love2work" },
{ "id": "1004", "company": "One Piece" },
];


let employeeDatabase = [];
for (let i in students) {
var obj = {};
for (let s in students[i]) {
    obj[s] = students[i][s];
}
for (let c in company[i]) {
    obj[c] = company[i][c];
}
for (let sa in salary[i]) {
    obj[sa] = salary[i][sa];
}
for (let l in like[i]) {
    obj[l] = like[i][l];
}
for (let dl in dislike[i]) {
    obj[dl] = dislike[i][dl];
}
console.log(obj);
employeeDatabase.push(obj);
}

let emp = JSON.stringify(employeeDatabase);

let file = require("fs");
function write(text) {
return new Promise(function (res, rej) {
    file.writeFile("homework3.txt", text, "utf8", function (error) {
        if (error) {
            rej(error);
        } else {
            res();
            console.log("Write Complete");
        }
    });
})
}

async function writeText(text) {
try {
    await write(text);
} catch (error) {
    console.error(error);
}
}

writeText(emp);