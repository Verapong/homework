let students =
    [
        { "id": "1001", 'firstname': 'Luke', 'lastname': 'Skywalker' },
        { "id": "1002", 'firstname': 'Tony', 'lastname': 'Stark' },
        { "id": "1003", 'firstname': 'Somchai', 'lastname': 'Jaidee' },
        { "id": "1004", 'firstname': 'Monkey D', 'lastname': 'Luffee' },
    ];

let company = [
    { "id": "1001", "company": "Walt Disney" },
    { "id": "1002", "company": "Marvel" },
    { "id": "1003", "company": "Love2work" },
    { "id": "1004", "company": "One Piece" },
];
let salary = [
    { "id": "1001", "salary": "40000" },
    { "id": "1002", "salary": "1000000" },
    { "id": "1003", "salary": "20000" },
    { "id": "1004", "salary": "9000000" },
];
let like = [
    { "id": "1001", "like": "apple" },
    { "id": "1002", "like": "banana" },
    { "id": "1003", "like": "orange" },
    { "id": "1004", "like": "papaya" },
];
let dislike = [
    { "id": "1001", "dislike": "banana" },
    { "id": "1002", "dislike": "orange" },
    { "id": "1003", "dislike": "papaya" },
    { "id": "1004", "dislike": "apple" },
];
let employeeDatabase = [];
for (let a in students) {  // a เข้าไปใน student[]  
    let objStudents = {} //กำหนด obj วนลูปตามจำนวนคน [{มี4คน}]
    for (const s in students[a]) { // ให้s เข้าไปใน students obj[a]         
        objStudents[s] = students[a][s] //เก็บค่าจาก student obj[a] มาไว้ใน obj students รอบแรกได้ id รอบ2 ได้ firstname รอบ3ได้ lastname
    }

    for (const c in company[a]) {//วนลูปใน obj company
        objStudents[c] = company[a][c]//เก็บค่าจาก obj company
    }
    for (const p in salary[a]) {
        objStudents[p] = salary[a][p]
    }
    for (const l in like[a]) {
                objStudents[l] = like[a][l]
    }
    for (const d in dislike[a]) {
              objStudents[d] = dislike[a][d]
    }
       employeeDatabase.push(objStudents) // เก็บไว้ใน employeedata
}

console.log(employeeDatabase)

employeeDatabase = JSON.stringify(employeeDatabase) 

let fs = require('fs');
fs.writeFileSync('employeeDatabase.json',employeeDatabase,'utf8',function(err,data){
if(err){
    console.log(err)
}
else{
    console.log('compleat')
}
})
