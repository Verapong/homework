const { Employee } = require('./employee.js');
class Programmer extends Employee {
    constructor(firstname, lastname, salary, id, type) {
        super(firstname, lastname, salary);
        this.id = id
        this.type = type

    }
    work() {
        this._CreateWebsite();
        this._FixPC();
        this._InstallWindows();

    }

    _CreateWebsite() {

        console.log(this.firstname + 'is creat website.')
    }

    _FixPC() {

        console.log(this.firstname + 'is fixPc.')
    }

    _InstallWindows() {
        console.log(this.firstname + ' is install window.')

    }
}
exports.Programmer = Programmer;
