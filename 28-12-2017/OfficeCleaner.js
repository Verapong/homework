const { Employee } = require('./employee.js');
class Officecleaner extends Employee {
    constructor(firstname, lastname, salary,id, dressCode) {
        super(firstname, lastname, salary);
        this.id = id
        this.dressCode = dressCode

    }
    work() {
        this._Clean();
        this._KillCoachroach();
        this._DecorateRoom();
        this._WelcomeGuest();

    }

    _Clean() {

        console.log(this.firstname+' Clean')
    }

    _KillCoachroach() {

        console.log(this.firstname+' KillCoachroach')
    }

    _DecorateRoom() {
        console.log(this.firstname+' DecorateRoom')

    }

    _WelcomeGuest(){
        console.log(this.firstname+' WelcomeGuest')
    }
}
exports.Officecleaner = Officecleaner;
