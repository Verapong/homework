const koa = require('koa');
const ejs = require('Koa-ejs');
const app = new koa();

render(app,{
    root:path.join(_dirname,'view'),
    layout:'template',
    viewExt:'ejs',
    cache:false,
    debug:true
})
app.use(async (ctx,next) => {
    ctx.body = 'Hello world'
    await ctx.render("user",{
        "firstname":"Somchai",
        "lastname":"Jaidee"
    })
});