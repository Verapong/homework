const koa = require('koa')
const multer = require('koa-multer')
const jimp = require('jimp')
const upload = multer({ dest: 'upload/' })
const mysql = require('mysql2/promise')
const app = new koa()

function getconnection() {
    const pool = mysql.createPool({
        host: 'localhost',
        user: "root",
        database: "twitter"
    })
    return pool;
}
app.use(async (ctx) => {

    const db = await getconnection()

    await upload.single('file')(ctx)
    const tempFile = ctx.req.file.path;
    console.log(ctx.req.file.path)
    const outFile = tempFile + '.jpg'
    await jimp.read(tempFile)
        .then(img => {
            img.resize(250, 250).write(outFile)
            db.execute(`insert into tweet_photos (url) values (?)`, [outFile])

        })

    ctx.body = outFile
})



app.listen(3000)