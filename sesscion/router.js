const Koa = require('koa')
const session = require('koa-session')
const mysql2 = require('mysql2/promise')
let db = mysql2.createPool({
    host:'127.0.0.1',
    user:'root',
    database:'session'
})

const sessionConfig = {
    key: 'sess',
    maxAge: 10000,
    httpOnly: true, 
    store: {
        async get(key, maxAge, { rolling }) {
            try{
                let [session] = await db.execute(`select s_key ,value from list where s_key = ?` , [key])
                console.log(session[0].value)
                return JSON.parse(session[0].value)
            }
            catch (err) {
                
            }
            
        },
        async set(key, sess, maxAge, { rolling }) {
            try {
                await db.execute(`insert into list (s_key, value) values (?,?)`, [key , JSON.stringify(sess)])        
            } catch (error) {
                await db.execute('update list set value = ? where s_key = ?', [JSON.stringify(sess), key])
            }
        },
        async destroy(key) {
            await db.execute(`delete from list where s_key = ?`, [key])
        }
    }
}
const app = new Koa()
app.keys = ['supersecret']
app
    .use(session(sessionConfig, app))
    .use(handler)
    .listen(3000)
async function handler(ctx) {
    /* let pool =await  mysql2.createPool({
        host:'127.0.0.1',
        user:'root',
        database:'session'
    })
    let db = await pool.getConnection() */
    let n = ctx.session.views || 0
    ctx.session.views = ++n
    ctx.body = `${n} views`

    // console.log(sessionStore)
    
}

