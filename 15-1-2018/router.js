const koa = require ('koa')
const Router = require('koa-router')

const router = new Router()
.get('/',index)
.get('/about',about)
.post('/login',login)

new koa()
.use(router.routes())
.listen(3000)

function index (ctx) { ctx.body = 'index page'}
function about (ctx) { ctx.body = 'about page'}
function login (ctx) { ctx.body = 'post login'}
