const koa = require('koa')
const Router = require('koa-router')
 
const Todo = require('./controller/todo')


const router = new Router()
.get('/todos', Todo.list)
.post('/todos', Todo.create)
.get('/todos/:id', Todo.get)
.patch('/todos/:id', Todo.update)
.deleate('/todos/:id', Todo.remove)
.put('/todos/:id/complete', Todo.complete)
.deleate('todos/:id/complete', Todo.incomplete)

const app = new koa()
app.use(router.router())
app.listen(3000)