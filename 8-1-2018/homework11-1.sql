-- การบ้านข้อ 1
create database bookstore;

create table employees (
employee_id int,
firstname varchar(50),
lastname varchar(50),
age int,
add_employee timestamp default now(),
primary key (id),
 );


create table book (
ISBN VARCHAR (50),
bookname varchar(50),
price int,
add_book TIMESTAMP default now(),
primary key (ISBN),
 );

 create table sale_list (
ISBN VARCHAR (50),
employees_name varchar(50),
price_sale int,
quantity int,
date_sale TIMESTAMP default now(),
primary key (ISBN),
 );

-- การบ้านข้อ2
insert into employees (firstname,lastname,age) values
('Anakin','Skywallker',28),
('Bob','Deck',30),
('john','Mavin',22),
('David','Webb',30),
('Stacy','Bush',18),
('Joy','Narak',20),
('Fah','Beauty',19),
('Suay','Sudsoy',16),
('Jason','Broun',33),
('Matin','Peach',35);

delete from employees
where employee_id = 5;

alter table employees
add column address varchar(255);

select count(*) from employees;

select firstname from employees
where age <20 ;


--การบ้านข้อ 3
--เพมหนงสอเขาไป 10 เลม
insert into book (ISBN,bookname,price) values
('9780070141653','security analysis',850),
('9780070142653','harry potter',900),
('9780070143653','technica',1200),
('9785693285191','think and grow rich',960),
('9851151621627','The Hunger Games',750),
('8946451915191','To Kill a Mockingbird',871),
('7916519156567','Twilight',695),
('5951981881515','Gone with the Wind',785),
('8119115611619','The Fault in Our Stars',1253),
('1191561919161','The Giving Tree',895);

--คนหาหนงสอจากชอ เชน JavaScript ใหคนวา Script กตองหาเจอ
select bookname from book WHERE bookname LIKE '%a%'

--ดงหนงสอมาแสดง 4 เลมแรก ทชอมตวอกษร a
select bookname from book WHERE bookname LIKE '%a%' LIMIT 4

--เพมรายการขายหนงสอ 20 รายการ
insert into sale_list (ISBN,employees_name,price_sale,quantity) values
('9780070141653','Anakin',850,2),
('9780070142653','Anakin',900,2),
('9780070143653','Anakin',1200,1),
('9785693285191','Anakin',960,3),
('9851151621627','Anakin',750,4),
('8946451915191','Anakin',871,1),
('7916519156567','Anakin',695,8),
('5951981881515','Anakin',785,4),
('8119115611619','Anakin',1253,2),
('1191561919161','Anakin',895,1),
('9780070141653','john',850,2),
('9780070142653','john',900,2),
('9780070143653','john',1200,1),
('9785693285191','john',960,3),
('9851151621627','john',750,4),
('8946451915191','john',871,1),
('7916519156567','john',695,8),
('5951981881515','john',785,4),
('8119115611619','john',1253,2),
('1191561919161','john',895,1);


--หาวาขายหนงสอไดกเลม
SELECT sum(quantity) FROM sale_list

--หาวามหนงสออะไรบางทขายออก (แสดงแค ISBN ของหนงสอ)
select distinct ISBN from sale_list;

--หาวาขายหนงสอไดเงนทงหมดเทาไร
SELECT sum(quantity*price_sale) as summary FROM sale_list

