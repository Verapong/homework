
const koa = require('koa')
const app = new koa()
const render = require('koa-ejs');
const path = require('path');
const db = require('./lib/db.js')()


render(app, {
    root: path.join(__dirname, 'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false,
    debug: true
});

require('./controller/routes.js')(app);


//console.log(Config.database) เรียกดูค่าในอะเรย์


   
app.listen(3000)