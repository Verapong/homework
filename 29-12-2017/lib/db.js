const mysql = require('mysql2')
const { Config } = require('../config/database')



class Database {
    constructor(host, user, database) {
        this.connection = mysql.createConnection({
            host: Config.host,
            user: Config.user,
            database: Config.database
        })
    }

    getUser() {
        return new Promise((resolve, reject) => {
            let sql = "SELECT * FROM user"
            this.connection.query(sql, (err, results) => {
                if (err) reject(err)
                else resolve(results)
            })
        })
    }
}

module.exports = function () {
    return new Database('127.0.0.1', 'root', 'codecamp')
} //จะได้ไม่ต้อง ประกาสตัวแปรใหม่ทุก ๆ หน้า   let db = new Database('sass' , 'df', '')