const db = require('../lib/db.js')()
const callfunction = require('../models/homework10_1')


module.exports = function(app) {
    app.use(async (ctx, next) => {
        try {
            let data = await callfunction.Queryuser()
            console.log(data)
            await ctx.render('user', { "message": data });
            await next();
        } catch (err) {
            ctx.status = 400
            ctx.body = `Uh-oh: ${err.message}`
        }
    })
}