// if else
/*let password = '1235';

if (password === '1234'){
    console.log('longin ok');
 }else {
     console.log('not ok')
 }*/


//while loop ทำซ้ำจนกว่าจะเป็นจริง ทำกี่ครั้งก็ตามเงื่อนไขที่ตั้ง
/*let i = 1;
 while (i <= 5 ){
 
   console.log('Hello ' + i)
    i++;    
}*/


// for ลูป 
/*let colorball = ['red','yellow','pink'];
for (let i = 0; i < colorball.length; i++){
    console.log(colorball[i]);
}*/

//break จะเป็นการทำงานที่ทำให้หลุดออกจากลูป
/*let colorball = ['red','yellow','pink'];
for (let i = 0; i < colorball.length; i++){
    if (colorball[i]=== 'yellow'){
        break;
    }
    console.log(colorball[i]);
}*/

//continue เป็นการทำงานทีเมื่อตรวจสอบเงื่อนไขแล้วก็จะข้ามการทำงานนั้นไป
/*let colorball = ['red', 'yellow', 'pink'];
for (let i = 0; i < colorball.length; i++) {
    if (colorball[i] === 'yellow') {
        continue;
    }
    console.log(colorball[i]);
}*/


/*/ tenary operator
let username = 'admin';
if (username === 'admin') {
    console.log('admin');
} else {
    console.log('member');
}
//tenary
let result = username === 'admin' ? 'admin' : 'member'; // หลังเครื่องหมายคำถามแปลว่า เป็นจริง หลัง ; แปลว่าเป็นเท็จ
console.log(result);*/


/*/function เขียนมาเพื่อการใช้งานซ้ำๆกัน จะเรียกใชักีครั้งก็ได้
function showData (){
    console.log('Hello 1');
    console.log('Hello 2');
}
showData();*/

//ฟังชั่นแบบมีพารามิเตอร์ หรือ อากิวเม้น
/*function showInfo(name,grade){
    console.log('Hello '+name+'Grade: '+grade);
}
showInfo('john',3.40);*/

//function return เวลาที่เราจะเอาค่าที่ return ออกมาต้องมีตัวแปรมารับ
/*function sum(x,y) {
    return x+y;
    }
let total = sum(8,9);
console.log(total);*/


// default parameter
/*function f(a,b = 'default',c = 3){
    return  `${a} -- ${b} -- ${c}`;
}

console.log(f(10,'joe'));
*/


//calling vs referencing
function f(a, b = 'default', c = 3) {
    return `${a} -- ${b} -- ${c}`;
}
//const me = f;  // เป็นการเอาฟั่งชั่นมาใส่ตัวแปร แล้วเรียกได้
//console.log(me());

let o = {
    neme: 'marry'
}

o.showData = f;    //เป็นการสร้าง property ใหม่ให้กับ obj แล้วยัดฟั่งชั่นลงไปใน property ใหม่ที่ทำการสร้าง
//console.log(o.showData());
//console.log(o);

//function to array
let num = [1, 2, 3];
num[0] = f;
//console.log(num[0]);
//console.log(num[0]());

//Destructuring Assignment  ถ้าออปเจ็คที่ พรอพเพอตี้ชื่ออะไร ตัวแปรชื่อต้องชื่อเดียวกันเพราะโปรแกรมจะหาไม่เจอ
const os = {
    name: 'Ubuntu',
    version: 16.40
};
let { name, version } = os;
//console.log(name);


//Destructuring array
const os2 = ['window 10', 10];
let [name2, version3] = os2;
//console.log(name2);
//console.log(version3);

//Destructuring Arguments
const os3 = {
    name: 'IOS',
    version: 10
};

/*function getOS({name,version}){
    return 'OS: '+ name + ' Version: '+ version;
}*/
//เรียกฟั่งชั่น  
//console.log(getOS(os3));

//new shorthand method (function)
const os4 = {
    name: 'IOS',
    version: 10,
    //getOS: function () {return 'OS'};
    getOS() { return 'OS'; }
};
//console.log(os4.getOS());

//fat arrow การเขียนฟั่งชั่นให้สั้นลง
const f1 = function () {           //เขียนแบบเต็ม
    return "Hello Function";
};
//arrow function ตัดคำว่า function กับ return  ออก
const f2 = () => 'Hello Function';
f2();
const f3 = function (name) {
    return name;
}
f3();
//arrow

const f4 = (name) => name;
f4();
//console.log(f4());

const f5 = function (x, y) {
    return x + y;
}
//arrow
f6 = (x, y) => x + y;
//console.log(f6(10,5));

//Reat parameter แปลงตัวแปรที่รับเข้ามา ไปเป็น array
function sum(...numbers) {
    return numbers.reduce((sum, num) => sum += num); //reduce เป็นการทำก่ารทบแล้วคืนค่าออกมา 
}
//console.log(sum(1,2,3,4,5));


//Spread operator กระจาย array ที่รับเข้ามา ไปเป็นตัวแปรแต่ละตัว
function sum2(a, b, c) {
    return a + b + c;
}
let numbers2 = [12, 56, 69];
//console.log(sum2(...numbers2)); //ค่าในอะเรย์เกินมาแต่ก็ทำงานได้ 

let arr = ['a', 'b', 'c'];


/*for of การใช่้ for of เป็นการดึงค่าใน อะเรย์ออกมา 

for (let v of arr) {
    console.log(v);
}

//for in  การใช้ FOR IN  เป็นการดึง อินเด็กออกมา
for (let v in arr) {
    //console.log(v);
}

//for each
arr.forEach(function (item, index) {
   // console.log(index + '=>' + item);
});

//for each arrow
//arr.forEach ((item ,index) => console.log(index + ' =>' + item));
*/

// push เป็นการเพิ่มสมาชิคเช้าไปที่ตัวสุดท้ายของ อะเรย์
let arr2 = ['a', 'b', 'c'];
arr2.push('d');
for (let v of arr2) {
   // console.log(v);
}
console.log('------------pop----------');
//pop เป็นการลบค่าตัวสุดท้ายในอะเรย์ออก 
arr2.pop()
for (let v of arr2) {
   // console.log(v);
}

//unshift เป็นการเพิ่มสมาชิกตัวแรกเข้าไปใน array
console.log('------------unshift-----------');
arr2.unshift('z');
for (let v of arr2) {
   // console.log(v);
}

// shift เป็นการลบสมาชิกตัวแรกในอะเรยออก
console.log('---------shift--------------');
arr2.shift();
for (let v of arr2){
   // console.log(v);
}

//concat() เป็นการเพิ่มสมาชิกที่ละหลายๆตัว ต่อตัวสุดท้าย โดยที่ อะเรย์ตัวแรกไม่ได้เปลียนแปลง เพราะเราต้องหาตัวแปรมารับค่า คือเพิ่มค่าเข้าไปในอะเรย์

console.log('---------unmodifiled--------------');
let arr3 = arr2.concat(['d','e','f']);
for (let v of arr3){
    //console.log(v);
}

// slice() เป็นการตัดสมาชิกใน อะเรย์ คำสั่งนี้จะมี 2 คำสั้ง ตัวแรกเป็นตำแหน่งที่เริ่มตัด ตัวทีสองเป็นตำแหน่งสุดท้าย ตัวสุดท้ายไม่เอา  ฟั่งชั่นนี้เป็นการโมดิไฟ ไม่มีผลต่่อข้อมูลเดิม จึงต้องหาตัวแปรมารับ ค่า
console.log('----------slice-----------------');
let arr4 = [1,2,3,4,5]
let arr5 = arr4.slice(1); //เป็นการเอาค่าตั้งแต่ตัวที่กำหนด ตัวอย่างก็จะได้ค่า 2 เพราะเป็นอะเรย์ตัวที่ 1 
for (let v of arr5){
   // console.log(v);
}

//การเพิ่มหรือลบสมาชิก(ได้ทุกตำแหน่ง) ใช้ splice() เป็นการแก้ไชตัวต้นฉบับของข้อมูลเลยไม่ต้องมีตัวแปรมารับ
//array.splice( ) 

console.log('----------splice-----------------');
let arr6 = [1,2,'a',5,6]
arr6.splice(2,1,3,4)  //argument ที่ 1 คือ เลข index ที่เราตองการเริ่มแกไข คือเริ่มตัวที่2 , argument ที่ 2 คือ จํานวน elements ที่จะเริ่มลบ (ถาไมลบใส 0) ตัวอย่างใส่ 1 ,argument ที่ 3 คือ elements ใหมที่ตองการจะเพิ่มเขาไป ตัวอย่างใส่ 4,5
for (let v of arr6){
    //console.log(v);
}

//การตัด และ ทับ ตำแหน่งเดิม copyWithin() copy value 
const arr7 = [1,2,3,4];
arr7.copyWithin(1,2);  // ตำแหน่งแรกคือ เราจะcopy ทับตำแหน่งอะไร ตัวอย่างทับตำแหน่งที่ 1 คือ ค่า2 , argument 2 คือจะcopy จากตำแหน่งอะไร ตัวอย่างคือ 2 
console.log(arr7);  // [1,3,4,4]

arr7.copyWithin(2,0,2);// argument ที่สาม  คือ copy ไปถึงตำแหน่งเท่าไร ตัวอย่างคือตำแหน่งที่ 2 
console.log(arr7);

arr7.copyWithin(0,-3,-1);
console.log(arr7);


// method fil() เป็นการเติมค่า array ด้วยค่าตายตัว

