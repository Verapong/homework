const mysql = require('mysql2/promise')

function connect() {
    return mysql.createConnection({
        host: 'localhost',
        user: 'root',
        database: 'codecamp'
    })
}

async function runHomework() {
    const db = await connect()

    const User = require('./user')(db)

    let allUser = await User.findAll()
    console.log(allUser)

    const user1 = await User.find(1001)
    user1.firstName = 'tester'
    user1.save()

    

    const user2 = await User.find(1001)
    user2.remove()
}

runHomework()