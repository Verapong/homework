class User {
    constructor(db, row) {
        this._db = db
        this.id = row.id
        this.firstName = row.firstname
        this.lastName = row.lastname
    }
    async save() {
        if (!this.id) {
            const result = this._db.execute(`insert into user (firstname, lastname) values (?, ?)`, [this.firstName, this.lastName])
            this.id = result.insertId
            return
        }

        return this._db.execute(`update user set firstname = ?, lastname = ? where id = ?`, [this.firstName, this.lastName, this.id])
    }
    remove() {
        return this._db.execute(`delete from user where id = ?`, [this.id])
    }
}

module.exports = function (db) {
    return {
        async find(id) {
            const [rows] = await db.execute(`select id, firstname, lastname from user where id = ?`, [id])
            return new User(db, rows[0])
        },
        async findAll() {
            const [rows] = await db.execute(`select id, firstname, lastname from user`)
            return rows.map((row) => new User(db, row))
        }
    }
}