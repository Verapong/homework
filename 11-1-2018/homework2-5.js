const mysql = require('mysql2/promise')

const pool = mysql.createPool({
    host: 'localhost',
    user: 'root',
    database: 'codecamp'
})

    ; (async function () {
        const db = await pool.getConnection()

        await db.beginTransaction()
        try {
            const [rows] = await db.execute(`
        delete from user 
        where id = 1007 `)
            console.log(rows)

            await db.commit()
            console.log(rows)
        } catch (err) {
            console.log(err)
            await db.rollback()
        }
        await db.release()
    })().then(
        () => { },
        (err) => { console.log(err) }
        )