const koa = require('koa')
const bodyparser = require('koa-bodyparser')
const bcrypt = require('bcryptjs')

const app = new koa()

app.use(bodyparser())
// app.use(async(ctx, next) => {
//     ctx.$$$throw = ctx.throw
//     ctx.throw = function (code, obj) {
//         ctx.status = code
//         ctx.body = obj
//         throw new Error('')
//     }
//     try {
//         next()
//     } catch (err) {
//         console.error(err)
//     }
// })

app.use(async(ctx) => {         //header
    const { password } = ctx.request.body
    console.log(ctx.request.body)
    if (!password) {
        ctx.throw(400)
    }

    const hash = await bcrypt.hash(password, 10)
    ctx.body = {hash}
    console.log(hash)
})

app.listen(3000)
