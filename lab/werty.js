

const koa = require('koa')

const app = new koa()

app.use((ctx) => {
    return connectDatabase()
    .then((db) => {
        ctx.body = db
    })
})

app.listen(3000)

function connectDatabase () {
    return new Promise ((resolve) => {
        setTimeout(() => {
            resolve('database : p')
        },1000)
    })
}