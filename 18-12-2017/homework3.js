let peopleSalary = [
    { "id": 1001, "firstname": "Luke", "Lastname": "Skywalker", "company": "Walt Disney", "salary": 40000 },
    { "id": 1002, "firstname": "Tony", "Lastname": "Stark", "company": "Marvel", "salary": 1000000 },
    { "id": 1003, "firstname": "Somchai", "Lastname": "Jaidee", "company": "Love2work", "salary": 20000 },
    { "id": 1004, "firstname": "Monkey D", "Lastname": "Luffee", "company": "One Piece", "salary": 9000000 }
];

let newPeopleSalary = new Array(); // [] equals new Array()

function calulateNewSalary(salaryIn) {
    return salaryIn + (salaryIn * 0.1);
}

function getFutureSalary(salaryIn) {
    let newFutureSalary = new Array(); // []
    for (let i = 0; i <= 2; i++) {
        if (newFutureSalary.length === 0) { 
            // เข้ามาทำในนี้ครั้งแรกเท่านั้น
            // i is 0
            newFutureSalary.push(calulateNewSalary(salaryIn));
            //  newFutureSalary.push(calulateNewSalary(salaryIn));
        } else {
            // ถ้าไม่ใ่ช่ครั้งแรกให้ใช้การคำนวนครั้งล่าสุด คือ เอาตำแหน่งปัจจุบัน ถอยไปหนึ่ง
            newFutureSalary.push(calulateNewSalary(newFutureSalary[i - 1]));
            // i is 1 => newFutureSalary[1-1]
            // newFutureSalary.push(calulateNewSalary(newFutureSalary[0]));
            // i is 2 => newFutureSalary[2-1]
            // newFutureSalary.push(calulateNewSalary(newFutureSalary[1]));
        }
    }
    return newFutureSalary;
}

for (index in peopleSalary) { // Getting index of array 0, 1, 2, 3
    // console.log("index: " + index);
    // 0 , 1,2,3
    // {"id":1001,"firstname":"Luke","Lastname":"Skywalker","company":"Walt Disney","salary":40000},
    let newObject = new Object(); // {}
    for (property in peopleSalary[index]) { // Getting propertyName of JSON eg. id, firstname, Lastname, company, salary
        if (property !== "company") {
            newObject[property] = peopleSalary[index][property];
        }
    }
    newObject["salary"] = getFutureSalary(newObject["salary"]);
    newPeopleSalary.push(newObject);

}


console.log("newPeopleSalary", newPeopleSalary);

// ["a","b","c"]
// {"name":"a"}

